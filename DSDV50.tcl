# This script is created by NSG2 beta1
# <http://wushoupong.googlepages.com/nsg>

#===================================
#     Simulation parameters setup
#===================================
#Antenna/OmniAntenna set Gt_ 1              ;#Transmit antenna gain
#Antenna/OmniAntenna set Gr_ 1              ;#Receive antenna gain
#Phy/WirelessPhy set L_ 1.0                 ;#System Loss Factor
#Phy/WirelessPhy set freq_ 2.472e9          ;#channel
#Phy/WirelessPhy set bandwidth_ 11Mb        ;#Data Rate
#Phy/WirelessPhy set Pt_ 0.031622777        ;#Transmit Power
#Phy/WirelessPhy set CPThresh_ 10.0         ;#Collision Threshold
#Phy/WirelessPhy set CSThresh_ 5.011872e-12 ;#Carrier Sense Power
#Phy/WirelessPhy set RXThresh_ 5.82587e-09  ;#Receive Power Threshold
#Mac/802_11 set dataRate_ 11Mb              ;#Rate for Data Frames
#Mac/802_11 set basicRate_ 1Mb              ;#Rate for Control Frames

set val(chan)   Channel/WirelessChannel    ;# channel type
set val(prop)   Propagation/TwoRayGround   ;# radio-propagation model
set val(netif)  Phy/WirelessPhy            ;# network interface type
set val(mac)    Mac/802_11                 ;# MAC type
set val(ifq)    Queue/DropTail/PriQueue    ;# interface queue type
set val(ll)     LL                         ;# link layer type
set val(ant)    Antenna/OmniAntenna        ;# antenna model
set val(ifqlen) 200                        ;# max packet in ifq
set val(nn)     50                          ;# number of mobilenodes
set val(rp)     DSDV                       ;# routing protocol
set val(x)      500                      ;# X dimension of topography
set val(y)      500                      ;# Y dimension of topography
set val(stop)   60.0                         ;# time of simulation end

#===================================
#        Initialization        
#===================================
#Create a ns simulator
set ns [new Simulator]

#Setup topography object
set topo       [new Topography]
$topo load_flatgrid $val(x) $val(y)
create-god $val(nn)

#Open the NS trace file
set tracefd [open dsdv_out50.tr w]
$ns trace-all $tracefd

$ns use-newtrace

#Open the NAM trace file
set namtrace [open dsdv_out50.nam w]
$ns namtrace-all-wireless $namtrace $val(x) $val(y)
set chan [new $val(chan)];#Create wireless channel


set chan_1 [new $val(chan)]
#===================================
#     Mobile node parameter setup
#===================================
$ns node-config -adhocRouting  $val(rp) \
                -llType        $val(ll) \
                -macType       $val(mac) \
                -ifqType       $val(ifq) \
                -ifqLen        $val(ifqlen) \
                -antType       $val(ant) \
                -propType      $val(prop) \
                -phyType       $val(netif) \
                -channel       $chan \
                -topoInstance  $topo \
             

#===================================
#        Nodes Definition        
#===================================
#Create 5 nodes

for {set i 0} {$i < 10} { incr i } {
            set n($i) [$ns node]
            $n($i) random-motion 0  
            $n($i) color red
            $ns at 0.0 "$n($i) color red"
            $ns initial_node_pos $n($i) 15*i+10
    }
for {set j 10} {$j < 20} { incr j } {
            set n($j) [$ns node]
            $n($j) random-motion 0  
            $n($j) color green
            $ns at 0.0 "$n($j) color green"
            $ns initial_node_pos $n($j) 15*j+10
    }
for {set k 20} {$k < 30} { incr k } {
            set n($k) [$ns node]
            $n($k) random-motion 0  
            $n($k) color blue
            $ns at 0.0 "$n($k) color blue"
            $ns initial_node_pos $n($k) 15*k+10
    }

for {set l 30} {$l < 50} { incr l } {
            set n($l) [$ns node]
            $n($l) random-motion 0  
            $n($l) color black
            $ns at 0.0 "$n($l) color black"
            $ns initial_node_pos $n($l) 15*l+10
    }

#===================================
#        Generate movement          
#===================================
$ns at 0.0 "$n(0) setdest 91.7 68.0 10000.0"
$ns at 0.5 "$n(1) setdest 28.4 168.3 10000.0"
$ns at 0.7 "$n(2) setdest 27.3 227.4 10000.0"
$ns at 5.0 "$n(3) setdest 20.05 3.98 10000.0"
$ns at 0.0 "$n(4) setdest 30.8 435.3 10000.0"
$ns at 0.0 "$n(5) setdest 166.3 64.0 10000.0"
$ns at 4.3 "$n(6) setdest 182.8 384.2 10000.0"
$ns at 0.0 "$n(7) setdest 104.5 226.2 10000.0"
$ns at 0.0 "$n(8) setdest 135.9 105.3 10000.0"
$ns at 4.0 "$n(9) setdest 104.0 436.3 10000.0"
$ns at 0.0 "$n(10) setdest 251.1 64.8 10000.0"
$ns at 5.0 "$n(11) setdest 251.9 118.3 10000.0"
$ns at 0.0 "$n(12) setdest 173.2 224.6 10000.0"
$ns at 0.0 "$n(13) setdest 249.8 6.7 10000.0"
$ns at 0.5 "$n(14) setdest 242.8 437.0 10000.0"
$ns at 2.0 "$n(15) setdest 413.6 70.3 10000.0"
$ns at 0.0 "$n(16) setdest 204.0 286.0 10000.0"
$ns at 5.0 "$n(17) setdest 418.9 215.4 10000.0"
$ns at 0.0 "$n(18) setdest 110.5 361.3 10000.0"
$ns at 3.0 "$n(19) setdest 175.1 434.9 10000.0"
$ns at 0.0 "$n(20) setdest 249.7 319.0 10000.0"
$ns at 0.0 "$n(21) setdest 248.8 248.1 10000.0"
$ns at 0.0 "$n(22) setdest 344.2 219.1 10000.0"
$ns at 0.0 "$n(23) setdest 360.9 105.5 10000.0"
$ns at 0.0 "$n(24) setdest 473.9 440.3 10000.0"
$ns at 0.0 "$n(25) setdest 102.6 286.2 10000.0"
$ns at 4.0 "$n(26) setdest 483.3 11.7 10000.0"
$ns at 0.0 "$n(27) setdest 484.7 295.1 10000.0"
$ns at 0.0 "$n(28) setdest 485.8 67.9 10000.0"
$ns at 8.0 "$n(29) setdest 486.1 220.7 10000.0"
$ns at 0.0 "$n(30) setdest 191.7 168.0 10000.0"
$ns at 0.5 "$n(31) setdest 282.4 18.3 10000.0"
$ns at 0.7 "$n(32) setdest 27.3 217.4 10000.0"
$ns at 5.0 "$n(33) setdest 20.05 322.98 10000.0"
$ns at 0.0 "$n(34) setdest 30.8 45.3 10000.0"
$ns at 0.0 "$n(35) setdest 166.3 164.0 10000.0"
$ns at 4.3 "$n(36) setdest 182.8 84.2 10000.0"
$ns at 0.0 "$n(37) setdest 104.5 26.2 10000.0"
$ns at 0.0 "$n(38) setdest 135.9 5.3 10000.0"
$ns at 4.0 "$n(39) setdest 104.0 436.3 10000.0"
$ns at 0.0 "$n(40) setdest 91.7 168.0 10000.0"
$ns at 0.5 "$n(41) setdest 281.4 168.3 10000.0"
$ns at 0.7 "$n(42) setdest 271.3 227.4 10000.0"
$ns at 5.0 "$n(43) setdest 202.05 3.98 10000.0"
$ns at 0.0 "$n(44) setdest 302.8 435.3 10000.0"
$ns at 0.0 "$n(45) setdest 66.3 64.0 10000.0"
$ns at 4.3 "$n(46) setdest 82.8 384.2 10000.0"
$ns at 0.0 "$n(47) setdest 14.5 226.2 10000.0"
$ns at 0.0 "$n(48) setdest 35.9 105.3 10000.0"
$ns at 4.0 "$n(49) setdest 10.0 436.3 10000.0"

$ns at 10.3 "$n(8) setdest 91.7 68.0 50.0"
$ns at 10.3 "$n(2) setdest 28.4 168.3 50.0"
$ns at 10.5 "$n(31) setdest 28.4 181.3 10000.0"
$ns at 10.7 "$n(32) setdest 270.3 27.4 10000.0"
$ns at 10.3 "$n(9) setdest 30.8 435.3 50.0"
$ns at 10.3 "$n(0) setdest 166.3 64.0 50.0"
$ns at 10.3 "$n(25) setdest 104.5 226.2 50.0"
$ns at 14.3 "$n(36) setdest 181.8 184.2 10000.0"
$ns at 10.0 "$n(37) setdest 134.5 126.2 10000.0"
$ns at 8.3 "$n(19) setdest 104.0 436.3 50.0"
$ns at 10.0 "$n(30) setdest 11.7 18.0 10000.0"
$ns at 15.0 "$n(33) setdest 200.05 22.98 10000.0"
$ns at 10.0 "$n(38) setdest 13.9 15.3 10000.0"
$ns at 14.0 "$n(39) setdest 124.0 46.3 10000.0"
$ns at 10.0 "$n(40) setdest 191.7 168.0 10000.0"
$ns at 15.0 "$n(43) setdest 402.05 3.98 10000.0"
$ns at 14.3 "$n(46) setdest 182.8 384.2 10000.0"
$ns at 14.0 "$n(49) setdest 110.0 436.3 10000.0"
$ns at 10.3 "$n(5) setdest 251.1 64.8 50.0"
$ns at 10.3 "$n(11) setdest 251.9 118.3 50.0"
$ns at 9.3 "$n(6) setdest 110.5 361.3 50.0"
$ns at 10.3 "$n(14) setdest 175.1 434.9 50.0"
$ns at 10.0 "$n(34) setdest 300.8 450.3 10000.0"
$ns at 10.0 "$n(35) setdest 163.3 167.0 10000.0"
$ns at 13.3 "$n(20) setdest 249.7 319.0 50.0"
$ns at 10.3 "$n(16) setdest 248.8 248.1 50.0"
$ns at 10.5 "$n(41) setdest 481.4 168.3 10000.0"
$ns at 10.7 "$n(42) setdest 171.3 227.4 10000.0"
$ns at 16.3 "$n(15) setdest 360.9 105.5 50.0"
$ns at 10.3 "$n(12) setdest 102.6 286.2 50.0"
$ns at 19.3 "$n(29) setdest 484.7 295.1 50.0"
$ns at 10.3 "$n(26) setdest 485.8 67.9 50.0"
$ns at 11.3 "$n(28) setdest 487.0 131.0 50.0"
$ns at 10.3 "$n(22) setdest 414.9 146.4 50.0"
$ns at 12.3 "$n(1) setdest 26.5 113.7 50.0"
$ns at 10.3 "$n(13) setdest 323.7 8.7 50.0"
$ns at 16.3 "$n(3) setdest 101.2 4.0 50.0"
$ns at 10.3 "$n(23) setdest 330.7 142.1 50.0"
$ns at 10.3 "$n(27) setdest 477.8 374.5 50.0"
$ns at 10.0 "$n(44) setdest 302.8 435.3 10000.0"
$ns at 10.0 "$n(45) setdest 166.3 64.0 10000.0"
$ns at 10.3 "$n(7) setdest 111.5 168.7 50.0"
$ns at 10.3 "$n(17) setdest 419.6 271.8 50.0"
$ns at 18.3 "$n(24) setdest 388.4 438.1 50.0"
$ns at 10.3 "$n(10) setdest 325.5 60.5 50.0"
$ns at 10.0 "$n(47) setdest 144.5 226.2 10000.0"
$ns at 10.0 "$n(48) setdest 135.9 105.3 10000.0"
$ns at 15.3 "$n(4) setdest 29.9 372.5 50.0"
$ns at 13.3 "$n(18) setdest 161.8 318.8 50.0"
$ns at 18.3 "$n(21) setdest 247.6 201.6 50.0"

$ns at 20.0 "$n(30) setdest 194.5 226.2 10000.0"
$ns at 24.0 "$n(40) setdest 15.9 105.3 10000.0"
$ns at 23.8 "$n(5) setdest 91.7 68.0 50.0"
$ns at 25.8 "$n(8) setdest 104.5 226.2 50.0"
$ns at 24.8 "$n(0) setdest 135.9 105.3 50.0"
$ns at 21.8 "$n(14) setdest 104.0 436.3 50.0"
$ns at 23.8 "$n(12) setdest 251.1 64.8 50.0"
$ns at 20.8 "$n(23) setdest 251.9 118.3 50.0"
$ns at 25.8 "$n(25) setdest 173.2 224.6 50.0"
$ns at 18.0 "$n(31) setdest 234.5 216.2 10000.0"
$ns at 25.0 "$n(41) setdest 156.9 15.3 10000.0"
$ns at 25.8 "$n(16) setdest 204.0 286.0 50.0"
$ns at 26.8 "$n(18) setdest 110.5 361.3 50.0"
$ns at 20.8 "$n(20) setdest 248.8 248.1 50.0"
$ns at 21.8 "$n(15) setdest 360.9 105.5 50.0"
$ns at 23.8 "$n(27) setdest 483.3 11.7 50.0"
$ns at 25.8 "$n(29) setdest 484.7 295.1 50.0"
$ns at 27.8 "$n(28) setdest 486.1 220.7 50.0"
$ns at 25.8 "$n(21) setdest 414.9 146.4 50.0"
$ns at 29.8 "$n(2) setdest 26.5 113.7 50.0"
$ns at 27.0 "$n(32) setdest 209.5 126.2 10000.0"
$ns at 25.0 "$n(42) setdest 190.9 165.3 10000.0"
$ns at 23.8 "$n(24) setdest 316.5 437.7 50.0"
$ns at 25.8 "$n(3) setdest 101.2 4.0 50.0"
$ns at 25.8 "$n(1) setdest 23.6 63.1 50.0"
$ns at 21.8 "$n(13) setdest 407.6 9.1 50.0"
$ns at 24.0 "$n(33) setdest 494.5 226.2 100.0"
$ns at 26.0 "$n(43) setdest 315.9 105.3 100.0"
$ns at 23.8 "$n(4) setdest 30.3 293.5 50.0"
$ns at 28.8 "$n(6) setdest 395.3 363.4 50.0"
$ns at 20.8 "$n(29) setdest 477.8 374.5 50.0"
$ns at 29.8 "$n(17) setdest 419.6 271.8 50.0"
$ns at 25.0 "$n(34) setdest 44.5 26.2 100.0"
$ns at 24.0 "$n(44) setdest 157.9 15.3 345.0"
$ns at 28.8 "$n(22) setdest 325.5 60.5 50.0"
$ns at 26.8 "$n(9) setdest 29.9 372.5 50.0"
$ns at 25.0 "$n(35) setdest 104.5 286.2 700.0"
$ns at 26.0 "$n(45) setdest 195.9 165.3 500.0"
$ns at 27.8 "$n(11) setdest 247.6 201.6 50.0"
$ns at 27.0 "$n(36) setdest 494.5 286.2 600.0"
$ns at 28.0 "$n(46) setdest 415.9 205.3 900.0"
$ns at 20.0 "$n(38) setdest 134.5 226.2 70000.0"
$ns at 30.0 "$n(48) setdest 156.9 105.3 7000.0"
$ns at 31.0 "$n(39) setdest 234.5 226.2 400.0"
$ns at 21.0 "$n(49) setdest 123.9 105.3 400.0"
$ns at 20.0 "$n(37) setdest 99.5 299.2 900.0"
$ns at 20.0 "$n(47) setdest 158.9 99.3 9000.0"

$ns at 40.0 "$n(14) setdest 27.3 227.4 5000.0"
$ns at 40.0 "$n(26) setdest 166.3 64.0 750.0"
$ns at 40.0 "$n(19) setdest 182.8 384.2 950.0"
$ns at 40.0 "$n(4) setdest 104.5 226.2 500.0"
$ns at 40.0 "$n(0) setdest 135.9 105.3 500.0"
$ns at 40.0 "$n(24) setdest 249.8 6.7 5000.0"
$ns at 40.0 "$n(2) setdest 242.8 437.0 1000.0"
$ns at 40.0 "$n(10) setdest 413.6 70.3 50.0"
$ns at 40.0 "$n(16) setdest 204.0 286.0 80.0"
$ns at 34.0 "$n(1) setdest 418.9 215.4 100.0"
$ns at 41.0 "$n(23) setdest 110.5 361.3 250.0"
$ns at 40.0 "$n(15) setdest 175.1 434.9 350.0"
$ns at 40.0 "$n(5) setdest 249.7 319.0 450.0"
$ns at 40.0 "$n(8) setdest 473.9 440.3 550.0"
$ns at 40.0 "$n(7) setdest 102.6 286.2 500.0"
$ns at 40.0 "$n(11) setdest 483.3 11.7 501.0"
$ns at 40.0 "$n(20) setdest 484.7 295.1 50.0"
$ns at 46.0 "$n(28) setdest 485.8 67.9 502.0"
$ns at 45.0 "$n(27) setdest 487.0 131.0 502.0"
$ns at 44.0 "$n(29) setdest 414.9 146.4 503.0"
$ns at 40.0 "$n(30) setdest 191.7 168.0 10000.0"
$ns at 40.5 "$n(31) setdest 282.4 18.3 10000.0"
$ns at 40.7 "$n(32) setdest 27.3 217.4 10000.0"
$ns at 45.0 "$n(33) setdest 20.05 322.98 10000.0"
$ns at 40.0 "$n(34) setdest 30.8 45.3 10000.0"
$ns at 40.0 "$n(35) setdest 166.3 164.0 10000.0"
$ns at 44.3 "$n(36) setdest 182.8 84.2 10000.0"
$ns at 40.0 "$n(37) setdest 104.5 26.2 10000.0"
$ns at 40.0 "$n(38) setdest 135.9 5.3 10000.0"
$ns at 44.0 "$n(39) setdest 104.0 436.3 10000.0"
$ns at 40.0 "$n(40) setdest 91.7 168.0 10000.0"
$ns at 40.5 "$n(41) setdest 281.4 168.3 10000.0"
$ns at 40.7 "$n(42) setdest 271.3 227.4 10000.0"
$ns at 45.0 "$n(43) setdest 202.05 3.98 10000.0"
$ns at 40.0 "$n(44) setdest 302.8 435.3 10000.0"
$ns at 40.0 "$n(45) setdest 66.3 64.0 10000.0"
$ns at 44.3 "$n(46) setdest 82.8 384.2 10000.0"
$ns at 40.0 "$n(47) setdest 14.5 226.2 10000.0"
$ns at 40.0 "$n(48) setdest 35.9 105.3 10000.0"
$ns at 44.0 "$n(49) setdest 10.0 436.3 10000.0"
$ns at 43.0 "$n(6) setdest 323.7 8.7 150.0"
$ns at 33.0 "$n(18) setdest 316.5 437.7 560.0"
$ns at 40.0 "$n(21) setdest 243.9 374.2 500.0"
$ns at 41.0 "$n(18) setdest 419.6 271.8 5000.0"
$ns at 42.0 "$n(25) setdest 304.3 281.9 5000.0"
$ns at 43.0 "$n(12) setdest 325.5 60.5 500.0"
$ns at 44.0 "$n(22) setdest 318.5 382.1 500.0"
$ns at 45.0 "$n(17) setdest 29.9 372.5 5000.0"


$ns at 50.0 "$n(0) setdest 91.7 68.0 10000.0"
$ns at 50.5 "$n(1) setdest 28.4 168.3 10000.0"
$ns at 50.7 "$n(2) setdest 27.3 227.4 10000.0"
$ns at 50.0 "$n(3) setdest 20.05 3.98 10000.0"
$ns at 50.0 "$n(4) setdest 30.8 435.3 10000.0"
$ns at 50.0 "$n(5) setdest 166.3 64.0 10000.0"
$ns at 50.3 "$n(6) setdest 182.8 384.2 10000.0"
$ns at 50.0 "$n(7) setdest 104.5 226.2 10000.0"
$ns at 50.0 "$n(8) setdest 135.9 105.3 10000.0"
$ns at 50.0 "$n(9) setdest 104.0 436.3 10000.0"
$ns at 50.0 "$n(10) setdest 251.1 64.8 10000.0"
$ns at 50.0 "$n(11) setdest 251.9 118.3 10000.0"
$ns at 50.0 "$n(12) setdest 173.2 224.6 10000.0"
$ns at 50.0 "$n(13) setdest 249.8 6.7 10000.0"
$ns at 50.5 "$n(14) setdest 242.8 437.0 10000.0"
$ns at 52.0 "$n(15) setdest 413.6 70.3 10000.0"
$ns at 50.0 "$n(16) setdest 304.0 286.0 10000.0"
$ns at 50.0 "$n(30) setdest 191.7 168.0 10000.0"
$ns at 50.5 "$n(31) setdest 282.4 18.3 10000.0"
$ns at 50.7 "$n(32) setdest 127.3 217.4 10000.0"
$ns at 50.0 "$n(33) setdest 120.05 322.98 10000.0"
$ns at 50.0 "$n(34) setdest 130.8 45.3 10000.0"
$ns at 50.0 "$n(35) setdest 266.3 164.0 10000.0"
$ns at 50.3 "$n(36) setdest 282.8 84.2 10000.0"
$ns at 50.0 "$n(37) setdest 204.5 26.2 10000.0"
$ns at 50.0 "$n(38) setdest 435.9 5.3 10000.0"
$ns at 50.0 "$n(39) setdest 304.0 436.3 10000.0"
$ns at 50.0 "$n(40) setdest 291.7 168.0 10000.0"
$ns at 50.5 "$n(41) setdest 181.4 168.3 10000.0"
$ns at 50.7 "$n(42) setdest 171.3 227.4 10000.0"
$ns at 50.0 "$n(43) setdest 102.05 3.98 10000.0"
$ns at 50.0 "$n(44) setdest 102.8 435.3 10000.0"
$ns at 50.0 "$n(45) setdest 266.3 64.0 10000.0"
$ns at 50.3 "$n(46) setdest 282.8 384.2 10000.0"
$ns at 50.0 "$n(47) setdest 140.5 226.2 10000.0"
$ns at 50.0 "$n(48) setdest 350.9 105.3 10000.0"
$ns at 50.0 "$n(49) setdest 100.0 436.3 10000.0"
$ns at 50.0 "$n(17) setdest 418.9 215.4 10000.0"
$ns at 50.0 "$n(18) setdest 110.5 361.3 10000.0"
$ns at 50.0 "$n(19) setdest 175.1 434.9 10000.0"
$ns at 50.0 "$n(20) setdest 249.7 319.0 10000.0"
$ns at 50.0 "$n(21) setdest 248.8 248.1 10000.0"
$ns at 50.0 "$n(22) setdest 344.2 219.1 10000.0"
$ns at 50.0 "$n(23) setdest 360.9 105.5 10000.0"
$ns at 50.0 "$n(24) setdest 473.9 440.3 10000.0"
$ns at 50.0 "$n(25) setdest 102.6 286.2 10000.0"
$ns at 50.0 "$n(26) setdest 483.3 11.7 10000.0"
$ns at 50.0 "$n(27) setdest 484.7 295.1 10000.0"
$ns at 50.0 "$n(28) setdest 485.8 67.9 10000.0"
$ns at 48.0 "$n(29) setdest 486.1 220.7 10000.0"





#===================================
#        Agents Definition        
#===================================

##===================================
#        Agents Definition        
#===================================
set udp0 [new Agent/UDP]

$ns attach-agent $n(10) $udp0
set sink0 [new Agent/LossMonitor]
$ns attach-agent $n(1) $sink0
$ns connect $udp0 $sink0

set cbr0 [new Application/Traffic/CBR]
$cbr0 set packetSize_ 512
$cbr0 set rate_ 10kb
$cbr0 set random_ 1
$ns at 1.0 "$cbr0 start"
$ns at 60.0 "$cbr0 stop"


$cbr0 attach-agent $udp0

set udp1 [new Agent/UDP]
$ns attach-agent $n(20) $udp1

set sink1 [new Agent/LossMonitor]
$ns attach-agent $n(4) $sink1

$ns connect $udp1 $sink1


set cbr1 [new Application/Traffic/CBR]
$cbr1 set packetSize_ 512
$cbr1 set rate_ 10kb
$cbr1 set random_ 1
$cbr1 attach-agent $udp1
$ns at 1.0 "$cbr1 start"
$ns at 60.0 "$cbr1 stop"


set udp2 [new Agent/UDP]
$ns attach-agent $n(25) $udp2

set sink2 [new Agent/LossMonitor]
$ns attach-agent $n(16) $sink2


$ns connect $udp2 $sink2


set cbr2 [new Application/Traffic/CBR]
$cbr2 set packetSize_ 512
$cbr2 set rate_ 10kb
$cbr2 set random_ 1
$cbr2 attach-agent $udp2
$ns at 1.0 "$cbr2 start"
$ns at 60.0 "$cbr2 stop"

set udp3 [new Agent/UDP]
$ns attach-agent $n(3) $udp3

set sink3 [new Agent/LossMonitor]
$ns attach-agent $n(6) $sink3

$ns connect $udp3 $sink3

set cbr3 [new Application/Traffic/CBR]
$cbr3 set packetSize_ 512
$cbr3 set rate_ 10kb
$cbr3 set random_ 1
$cbr3 attach-agent $udp3
$ns at 1.0 "$cbr3 start"
$ns at 60.0 "$cbr3 stop"

#===================================
#        Termination        
#===================================
#Define a 'finish' procedure
proc finish {} {
        global ns tracefd namtrace
 	$ns flush-trace
        close $tracefd
	close $namtrace
	
	exec nam dsdv_out50.nam &
        exit 0

}
for {set i 0} {$i < $val(nn) } { incr i } {
    $ns at $val(stop) "\$n($i) reset"
}
$ns at $val(stop) "$ns nam-end-wireless $val(stop)"
$ns at $val(stop) "finish"
$ns at $val(stop) "puts \"done\" ; $ns halt"
$ns run

